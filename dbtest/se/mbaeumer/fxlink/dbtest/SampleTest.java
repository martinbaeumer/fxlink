package se.mbaeumer.fxlink.dbtest;

import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;

import org.dbunit.Assertion;
import org.dbunit.DBTestCase;
import org.dbunit.DatabaseUnitException;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

public class SampleTest extends DBTestCase
{
    private IDatabaseTester databaseTester;

	public SampleTest(String name)
    {
        super( name );
        
    }
    
    protected void setUp() throws Exception
    {
    	System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, "org.hsqldb.jdbcDriver" );
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, "jdbc:hsqldb:file:db/fxlink" );
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, "sa" );
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, "" );
    	System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, "public" );
    	
      /*  databaseTester = new JdbcDatabaseTester("org.hsqldb.jdbcDriver",
            "jdbc:hsqldb:file:db/fxlink", "sa", "");

        // initialize your dataset here
        IDataSet dataSet = getDataSet();
        // ...

        databaseTester.setDataSet( dataSet );
	// will call default setUpOperation
        databaseTester.onSetup();
        */
    }

    /*protected IDataSet getDataSet() throws Exception
    {    	
    	File f = new File("links.xml");
    	System.out.println(f.getAbsolutePath());
        return new FlatXmlDataSetBuilder().build(new FileInputStream("links.xml"));
    }
    */
    
    protected void tearDown(){
    	
    	try {
    		IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new File("test_delete.xml"));
			DatabaseOperation.DELETE.execute(getConnection(), expectedDataSet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    protected DatabaseOperation getTearDownOperation() throws Exception
    {    	
        return DatabaseOperation.DELETE_ALL;
    }
        
    @Test
    public void testMe() throws Exception
    {

        // Fetch database data after executing your code
        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTable = databaseDataSet.getTable("Link");

        // Load expected data from an XML dataset
        IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new File("links.xml"));
        ITable expectedTable = expectedDataSet.getTable("Link");

        // Assert actual database table match expected table
        Assertion.assertEquals(expectedTable, actualTable);
    }

    public void testDelete() throws Exception
    {    	
    	try {
    		IDataSet deletedDataSet = new FlatXmlDataSetBuilder().build(new File("test_delete.xml"));
			DatabaseOperation.DELETE.execute(getConnection(), deletedDataSet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        // Fetch database data after executing your code
        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTable = databaseDataSet.getTable("LinkTag");

        // Load expected data from an XML dataset
        IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new File("empty_linktag.xml"));
        ITable expectedTable = expectedDataSet.getTable("LinkTag");

        // Assert actual database table match expected table
        Assertion.assertEquals(expectedTable, actualTable);
    }

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream("links.xml"));
	}
}