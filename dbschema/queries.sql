-- select links and their categories
 select l.id as linkId, l.url, l.description as linkDescription, l. created as linkCreated, l.lastUpdated  as linkLastUpdated, l.categoryId as linkCategory, c.id as categoryId, c.name as category, c.description as categoryDescription, c.created as categoryCreated, c.lastUpdated as categoryLastUpdated
from link l left join category c on c.id = l.categoryId

---select all tags for a specific link
select lt.id as ltId, l.id as linkId, l.url, l.description, lt.tagid, t.name as tagname 
from linktag lt
left join link l on lt.linkid = l.id
left join tag t on lt.tagid = t.id
where linkId=1 

-- select all links for a specific tag
select l.id as linkId, l.url, l.description as linkDescription, l.created as linkCreated, l.lastUpdated as linklLastUpdated, lt.tagid as tagid
from linktag lt left join link l on lt.linkId=l.id
where tagid=6

