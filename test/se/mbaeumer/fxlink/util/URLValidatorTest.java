package se.mbaeumer.fxlink.util;

import org.junit.Test;

import junit.framework.TestCase;

public class URLValidatorTest extends TestCase {
	@Test
	public void testValidURL(){
		/*
		assertTrue(URLValidator.isValidURL("lotto.de"));
		assertFalse(URLValidator.isValidURL("www.ba"));
		assertFalse(URLValidator.isValidURL("www.b.de"));
		assertFalse(URLValidator.isValidURL("www.b.d"));
		assertTrue(URLValidator.isValidURL("www.ba.de"));
		assertTrue(URLValidator.isValidURL("ww.baum.de"));
		assertFalse(URLValidator.isValidURL("www.baume.d"));
		//assertFalse(URLValidator.isValidURL("www,bild.de"));
		assertFalse(URLValidator.isValidURL(",,,,,"));
		*/
		assertTrue(URLValidator.isValidURL("google.com"));
		assertTrue(URLValidator.isValidURL("http://www.java-forums.org/"));
		assertTrue(URLValidator.isValidURL("http://www.java-forums.org/newjava"));
		assertTrue(URLValidator.isValidURL("http://www.java-forums.org/newjavablalabla/"));
		assertTrue(URLValidator.isValidURL("http://www.java-forums.org/newjava/nonblockingcallbackmethod"));
		assertTrue(URLValidator.isValidURL("http://www.java-forums.org/new-java"));
		assertTrue(URLValidator.isValidURL("http://www.java-forums.org/new-java/71402-non-blocking-callback-method"));
		assertTrue(URLValidator.isValidURL("http://www.java-forums.org/new-java/71402-non-blocking-callback-method.html"));
		assertFalse(URLValidator.isValidURL("htpp://sport.com"));
		assertTrue(URLValidator.isValidURL("lott:pe"));
		assertTrue(URLValidator.isValidURL("lotto.co"));
		assertTrue(URLValidator.isValidURL("example.com"));
	}
}
