package se.mbaeumer.fxlink.api;

import java.sql.SQLException;

import se.mbaeumer.fxlink.handlers.GenericDBHandler;
import se.mbaeumer.fxlink.handlers.LinkTagDeletionDBHandler;

public class LinkTagHandler {
	public static void deleteAllLinkTags() throws SQLException{
		LinkTagDeletionDBHandler.deleteAllLinkTags(GenericDBHandler.getInstance());
	}
}
