package se.mbaeumer.fxlink.api;

import java.util.List;

import se.mbaeumer.fxlink.handlers.GenericDBHandler;
import se.mbaeumer.fxlink.handlers.ManagedItemDBHandler;

public class ManagedItemHandler {
	public static List<String> getManagedItems(){
		return ManagedItemDBHandler.getAllManagedItems(GenericDBHandler.getInstance());
	}
}
