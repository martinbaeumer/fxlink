package se.mbaeumer.fxlink.api;

import java.io.FileNotFoundException;
import java.sql.SQLException;

import javax.xml.stream.XMLStreamException;

import se.mbaeumer.fxlink.xmlexport.LinkXMLWriter;

public class XMLExportHandler {
	public static void exportData(String filename) throws FileNotFoundException, XMLStreamException, SQLException{
		LinkXMLWriter writer = new LinkXMLWriter(filename);
		writer.writeDataToFile();
	}
}
