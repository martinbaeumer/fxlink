package se.mbaeumer.fxlink.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SelectableTag {
	private StringProperty name = new SimpleStringProperty();
    private BooleanProperty selected = new SimpleBooleanProperty(false);
    private IntegerProperty id = new SimpleIntegerProperty();

    public String getName() {
        return name.get();
    }
    
    public void setName(String name){
    	this.name.set(name);
    }
    
    public StringProperty nameProperty() {
        return name;
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }
    public boolean isSelected() {
        return selected.get();
    }
    public void setSelected(boolean selected) {
        this.selected.set(selected);
    }
    
    public void setId(int id){
    	this.id.set(id);
    }
    
    public int getId(){
    	return id.get();
    }
}
